package com.uniandes.juanfdiaz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class NewSaleActivity extends AppCompatActivity {

    private TextInputLayout tilClient;
    private TextInputEditText etClient;
    private TextInputLayout tilAdress;
    private TextInputEditText etAdress;
    private TextInputLayout tilAmount;
    private TextInputEditText etAmount;
    private TextInputLayout tilNumber;
    private TextInputEditText etNumber;
    private TextInputLayout tilPeriodicity;
    private MaterialAutoCompleteTextView etPeriodicity;
    private TextInputLayout tilPart;
    private TextInputEditText etPart;
    private TextInputLayout tilDate;
    private TextInputEditText etDate;

    private Date selectedDate;

    private AppCompatButton btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_sale);

        initUI();
    }

    private void initUI(){



        //Calendar 1
        tilDate=findViewById(R.id.til_date);
        tilDate.setEndIconOnClickListener(v-> onDateClick());

        etDate=findViewById(R.id.et_date);
    }

    //Calendar 2
    private void onDateClick(){
        //Extra para seleccionar fecha anterior al seleccionar una
        if (selectedDate==null){
            Calendar cal= Calendar.getInstance(TimeZone.getTimeZone("UTC")); //Día de hoy
            cal.add(Calendar.DAY_OF_MONTH,1);
            selectedDate=cal.getTime();
        }

        //Extra para hacer que solo se seleccionen fechas posteriores a hoy
        Calendar cal= Calendar.getInstance(TimeZone.getTimeZone("UTC")); //Día de hoy
        CalendarConstraints constraints= new CalendarConstraints.Builder()
                .setValidator(DateValidatorPointForward.from(cal.getTimeInMillis()))
                .build();


        //Aqui ---Calendar 2-> los settings del calendario
        MaterialDatePicker<Long> datePicker= MaterialDatePicker.Builder
                .datePicker()
                .setSelection(selectedDate.getTime())
                .setCalendarConstraints(constraints)
                .setTitleText("Siguente pago")
                .build();

        datePicker.addOnPositiveButtonClickListener(new  MaterialPickerOnPositiveButtonClickListener<Long>() {
            @Override
            public void onPositiveButtonClick(Long selection) {
                Toast.makeText(NewSaleActivity.this,""+selection,Toast.LENGTH_SHORT).show();
                onDateSelected(selection);
            }
        });
        //datePicker.addOnPositiveButtonClickListener(this::onDateSelected); Una manera de resumir lineas de addonPositive.

        datePicker.show(getSupportFragmentManager(), "date");
    }

    //Calendar 3
    private void onDateSelected(Long selection){
        selectedDate= new Date(selection);
        etDate.setText(SimpleDateFormat.getDateInstance().format(selectedDate));
    }
}