package com.uniandes.juanfdiaz.model;

import com.uniandes.juanfdiaz.mvp.PaymentsMVP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PaymentsInteractor implements PaymentsMVP.Model {

    private List<PaymentsMVP.PaymentsInfo> data;

    public PaymentsInteractor(){
        data= Arrays.asList(
            new PaymentsMVP.PaymentsInfo("Luna Azul","Calle 20 #2A 10"),
            new PaymentsMVP.PaymentsInfo("Carlos Esteban","Carrera azul"),
            new PaymentsMVP.PaymentsInfo("Valeria Montero","Carrera 6ta avenida"),
            new PaymentsMVP.PaymentsInfo("Valentina Tequendama","24 de octubre")
        );
    }


    @Override
    public void loadPayments(LoadPaymentsCallback callback) {
        callback.showPaymentsInfo(data);
    }
}
