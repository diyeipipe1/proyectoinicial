package com.uniandes.juanfdiaz.model;

import android.content.Context;

import com.uniandes.juanfdiaz.model.database.entities.User;
import com.uniandes.juanfdiaz.model.repository.UserRepository;
import com.uniandes.juanfdiaz.mvp.LoginMVP;

import java.util.HashMap;
import java.util.Map;

public class LoginInteractor  implements LoginMVP.Model {

    private UserRepository userRepository;

    public LoginInteractor(Context context){

        userRepository=new UserRepository(context);

    }

    @Override
    public void validateCredentials(String email, String password, ValidateCredentialsCallback callback) {
        User user=userRepository.getUserByEmail(email);

        if (user == null) { //Primera Garantiza que correo exista, devuelve clave como valor
            callback.onFailure("Usuario no existe");
        } else if (!user.getPassword().equals(password)) {
            callback.onFailure("Contraseña incorrecta");
        } else {
            callback.onSuccess();
        }
    }
}
