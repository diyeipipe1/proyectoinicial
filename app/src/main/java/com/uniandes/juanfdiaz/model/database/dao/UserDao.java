package com.uniandes.juanfdiaz.model.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.uniandes.juanfdiaz.model.database.entities.User;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM User")
    List<User> getAll();

    @Query("SELECT * FROM User WHERE email= :email")
    User getUserByEmail(String email);

    @Insert
    void insert(User...users); //Recibe una lista de usuarios

    @Delete
    void delete(User user);
}
