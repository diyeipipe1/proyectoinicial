package com.uniandes.juanfdiaz.model.repository;

import android.content.Context;

import com.uniandes.juanfdiaz.model.database.SalesDatabase;
import com.uniandes.juanfdiaz.model.database.dao.UserDao;
import com.uniandes.juanfdiaz.model.database.entities.User;

public class UserRepository {

    private UserDao userDao;

    public UserRepository(Context context){

        userDao = SalesDatabase.getDatabase(context).getUserDao();

        loadInitialData();
    }

    private void loadInitialData(){
        userDao.insert(
                new User("Sebastian","se.ceballos@uniandes.edu.com","camilamejiacano"),
                new User("Juan Felipe","jf.diazr@uniandes.edu.co","marianamaderogomez")
        );
    }

    public User getUserByEmail(String email) {
        return userDao.getUserByEmail(email);
    }
}
