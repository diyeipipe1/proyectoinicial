package com.uniandes.juanfdiaz.model.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.uniandes.juanfdiaz.model.database.dao.UserDao;
import com.uniandes.juanfdiaz.model.database.entities.Role;
import com.uniandes.juanfdiaz.model.database.entities.User;

@Database(entities = {User.class, Role.class},version = 1)
public abstract class SalesDatabase extends RoomDatabase {

    public abstract UserDao getUserDao();

    //Como obtener una instancia de la base de datos
    private static volatile SalesDatabase INSTANCE;

    public static SalesDatabase getDatabase(Context context){ //Que sea capaz de devolverme un objeto del tipo SalesDatbase
        if (INSTANCE==null) {
            INSTANCE = Room.databaseBuilder(
                    context.getApplicationContext(),
                    SalesDatabase.class, "database-name") //Room para que cree un database a partir un objeto tipo SalesDatabase.class
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE; //Lo guarda en la variable instancia, si no existe la crea o si si solo la devuelve
    }


}
