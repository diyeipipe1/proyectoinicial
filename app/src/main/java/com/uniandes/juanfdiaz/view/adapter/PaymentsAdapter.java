package com.uniandes.juanfdiaz.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uniandes.juanfdiaz.R;
import com.uniandes.juanfdiaz.mvp.PaymentsMVP;

import java.util.ArrayList;
import java.util.List;

public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.ViewHolder> {

    private List<PaymentsMVP.PaymentsInfo> data;

    public PaymentsAdapter(){
        this.data= new ArrayList<>();
    }

    public void setData(List<PaymentsMVP.PaymentsInfo> data) {
        this.data=data;
        notifyDataSetChanged(); //Adapta los datos
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payments,parent,false);
        return new ViewHolder(view); //Crea el view
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PaymentsMVP.PaymentsInfo item= data.get(position); //Un elemento del array, el de la posición n.

        //holder.getIvClient().setImageIcon();
        holder.getTvClient().setText(item.getName());//Al holder la agrego los nombres y similares  desde la clase interna view holder abajo
        holder.getTvAddress().setText(item.getAddress()); //Se los pongo al holder que es un view de tarjeata

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView ivClient;
        private TextView tvClient;
        private TextView tvAddress;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivClient=itemView.findViewById(R.id.iv_client);
            tvClient=itemView.findViewById(R.id.tv_client);
            tvAddress=itemView.findViewById(R.id.tv_adress);
        }

        public ImageView getIvClient() {
            return ivClient;
        }

        public TextView getTvClient() {
            return tvClient;
        }

        public TextView getTvAddress() {
            return tvAddress;
        }
    }
}
