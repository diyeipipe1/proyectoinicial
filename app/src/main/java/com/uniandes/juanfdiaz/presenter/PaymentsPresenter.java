package com.uniandes.juanfdiaz.presenter;

import com.uniandes.juanfdiaz.model.PaymentsInteractor;
import com.uniandes.juanfdiaz.mvp.PaymentsMVP;

import java.util.List;

public class PaymentsPresenter implements PaymentsMVP.Presenter {
    private PaymentsMVP.View view;
    private PaymentsMVP.Model model;
    //EL PRESENTADOR CONECTA EL MODELO Y LA VISTA

    public PaymentsPresenter(PaymentsMVP.View view){
        this.view=view;
        this.model=new PaymentsInteractor();
    }

    @Override
    public void loadPayments() {
        new Thread(()->{
            model.loadPayments(new PaymentsMVP.Model.LoadPaymentsCallback() {
                @Override
                public void showPaymentsInfo(List<PaymentsMVP.PaymentsInfo> paymentsInfo) {
                    view.getActivity().runOnUiThread(()->{
                        view.showPaymentsInfo(paymentsInfo);
                    });
                }
            });
        }).start();
    }

    @Override
    public void onNewSaleClick() {
        view.onNewSaleClick();
    }
    //RECIVE LA VISTA Y EL MODELO CREO UNO NUEVO. La vista se ingresa desde la vista al ingresar this como parámetro. Pero esta clase, no la interfaz


}
