package com.uniandes.juanfdiaz.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.uniandes.juanfdiaz.model.LoginInteractor;
import com.uniandes.juanfdiaz.mvp.LoginMVP;

public class LoginPresenter implements LoginMVP.Presenter {

    LoginMVP.View view;
    LoginMVP.Model model;

    public LoginPresenter(LoginMVP.View view){
        this.view=view;
        this.model=new LoginInteractor(view.getActivity());
    }

    @Override
    public void loginWEmail() {
        boolean error= false;
        view.showEmailError("");
        view.showPasswordError("");

        LoginMVP.LoginInfo loginInfo= view.getLoginInfo();
        if(loginInfo.getEmail().trim().isEmpty()) {
            view.showEmailError("Correo electrónico es obligatorio");
            error=true;
        }else if (!isEmailValid(loginInfo.getEmail().trim())){
            view.showEmailError("Correo no es válido");
            error=true;
        }
        if(loginInfo.getPassword().trim().isEmpty()){
            view.showPasswordError("Contraseña es obligatoria");
            error=true;
        }else if (!isPasswordValid(loginInfo.getPassword().trim())){
            view.showPasswordError("Contraseña no cumple con criterios");
            error=true;
        }

        if (!error){
            view.startWaiting();
            new Thread(()-> {
                model.validateCredentials(loginInfo.getEmail().trim(), loginInfo.getPassword().trim(), new LoginMVP.Model.ValidateCredentialsCallback() {
                    @Override
                    public void onFailure(String error) {
                        view.getActivity().runOnUiThread(()->{
                            view.stopWaiting();
                            view.showGeneralError(error);
                        });
                    }

                    @Override
                    public void onSuccess() {
                        SharedPreferences preferences= view.getActivity()
                                .getSharedPreferences("Autentication", Context.MODE_PRIVATE);
                        preferences.edit()
                                .putBoolean("logged",true)
                                .apply(); //Queda un diccionario que recibe la preferencia autentication. Le da contexto privado, solo el cel sabe de eso.
                        // Arriba es el, al hacerlo ponga true en en la preferencia, valor interno llamado logged. Logged pertenecerá a la pregerencia Autentication preference.

                        view.getActivity().runOnUiThread(()->{
                            view.stopWaiting();
                            view.openPaymentsActivity();
                        });
                    }
                });
            }).start(); //Para la demora, manejarla como background
        }

    }

    public boolean isEmailValid(String email){
        return email.contains("@") && (email.endsWith(".com")||email.endsWith(".co"));
    }
    public boolean isPasswordValid(String password){
        return password.length() >= 8;
    }

    @Override
    public void loginWFacebook() {

    }

    @Override
    public void loginWGmail() {

    }

    @Override
    public void isLogged() {
        SharedPreferences preferences= view.getActivity()
                .getSharedPreferences("Autentication", Context.MODE_PRIVATE);
        boolean logs= preferences.getBoolean("logged",false);
        if (logs){
            view.openPaymentsActivity();
        }
    }
}
