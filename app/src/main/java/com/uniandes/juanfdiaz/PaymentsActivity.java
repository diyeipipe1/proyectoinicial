package com.uniandes.juanfdiaz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.uniandes.juanfdiaz.mvp.PaymentsMVP;
import com.uniandes.juanfdiaz.presenter.PaymentsPresenter;
import com.uniandes.juanfdiaz.view.adapter.PaymentsAdapter;

import java.util.List;

public class PaymentsActivity extends AppCompatActivity implements PaymentsMVP.View {

    private DrawerLayout drawerLayout;
    private MaterialToolbar topAppBar;
    private NavigationView navDrawer;

    //New but
    private RecyclerView rvPayments;
    private FloatingActionButton btnSale;


    //This is the new MVP migration
    private PaymentsMVP.Presenter presenter;

    //Adaptador
    private PaymentsAdapter paymentsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);

        presenter= new PaymentsPresenter(this);
        initUI();
        presenter.loadPayments();
    }

    private void initUI(){
        //Lo exótico de la drawer
        drawerLayout=findViewById(R.id.drawerLayout);

        topAppBar=findViewById(R.id.topAppBar);
        topAppBar.setNavigationOnClickListener(v ->drawerLayout.openDrawer(navDrawer));

        navDrawer=findViewById(R.id.nav_drawer);
        navDrawer.setNavigationItemSelectedListener(menuItem ->{
            menuItem.setCheckable(true);
            drawerLayout.closeDrawer(navDrawer);
            return true;
        });

        //Esto es para cargar la información en el recycler
        rvPayments=findViewById(R.id.recyclerView);
        rvPayments.setLayoutManager(new LinearLayoutManager(PaymentsActivity.this)); //Voy a colocar los elementos en una sola linea
        paymentsAdapter= new PaymentsAdapter();
        rvPayments.setAdapter(paymentsAdapter);


        btnSale= findViewById(R.id.btn_sale);
        btnSale.setOnClickListener(v-> presenter.onNewSaleClick());
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public void onNewSaleClick(){
        startActivity(new Intent(PaymentsActivity.this, NewSaleActivity.class));
    }

    @Override
    public void showPaymentsInfo(List<PaymentsMVP.PaymentsInfo> paymentsInfo) {
        paymentsAdapter.setData(paymentsInfo); //También para el recycler
    }
}