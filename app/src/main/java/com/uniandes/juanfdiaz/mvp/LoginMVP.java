package com.uniandes.juanfdiaz.mvp;
import android.app.Activity;

public interface LoginMVP {
    interface Model{
        void validateCredentials(String email, String password, ValidateCredentialsCallback callback);
        interface ValidateCredentialsCallback{
            void onFailure(String error);
            void onSuccess();
        }

    }

    interface Presenter{
        void loginWEmail();
        void loginWFacebook();
        void loginWGmail();
        void isLogged();
    }

    interface View{

        Activity getActivity();

        LoginInfo getLoginInfo();
        void showEmailError(String error);
        void showPasswordError(String error);
        void showGeneralError(String error);

        void clearData();

        void openPaymentsActivity();

        void startWaiting();
        void stopWaiting();
    }

    class LoginInfo{
        private String email;
        private String password;

        public LoginInfo(String email, String password){
            this.email=email;
            this.password=password;
        }

        public String getEmail(){
            return email;
        }

        public String getPassword(){
            return password;
        }
    }
}
