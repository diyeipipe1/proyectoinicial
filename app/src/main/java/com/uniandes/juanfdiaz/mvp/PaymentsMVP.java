package com.uniandes.juanfdiaz.mvp;

import android.app.Activity;

import java.util.List;

public interface PaymentsMVP {
    interface Model{
        void loadPayments(LoadPaymentsCallback callback); //Para no quedarse esperando manda esto

        interface LoadPaymentsCallback{ //Las dos posibles respuestas paymentsinfo, lista vacía o no
            void showPaymentsInfo(List<PaymentsInfo> paymentsInfo);
        }
    }

    interface Presenter{
        void loadPayments();
        void onNewSaleClick();
    }

    interface View{
        Activity getActivity();

        void onNewSaleClick();

        void showPaymentsInfo(List<PaymentsInfo> paymentsInfo);
    }

    class PaymentsInfo{
        private String image;
        private String name;
        private String address;

        public PaymentsInfo(String image, String name, String address){
            this.image=image;
            this.name=name;
            this.address=address;
        }

        public PaymentsInfo(String name, String address){
            this(null,name,address); //Llama al otro constructor.
        }

        public String getImage(){
            return image;
        }

        public String getName(){
            return name;
        }

        public String getAddress(){
            return address;
        }


    }
}
