package com.uniandes.juanfdiaz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.uniandes.juanfdiaz.mvp.LoginMVP;
import com.uniandes.juanfdiaz.presenter.LoginPresenter;

public class LoginActivity extends AppCompatActivity implements LoginMVP.View {

    private static final String EMAIL_KEY= "email";

    private LinearProgressIndicator piWaiting;

    private ImageView ivLogo;
    private TextInputEditText etExtra;
    private TextInputLayout tilExtra;
    private EditText etCorreo;
    private EditText etClave;
    private TextInputLayout tilEmail;
    private TextInputLayout tilPassword;

    private AppCompatButton btnLogin;
    private AppCompatButton btnFacebook;
    private AppCompatButton btnGoogle;

    //New con interfaz 1
    private LoginMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //New con interfaz 2, presenter no creado aún
        presenter= new LoginPresenter(this);

        presenter.isLogged();
        initUI();

    }

    private void initUI(){
        piWaiting=findViewById(R.id.pi_waiting);
        ivLogo=findViewById(R.id.iv_logo);
        etExtra=findViewById(R.id.et_extra);
        etCorreo=findViewById(R.id.et_correo);
        etClave=findViewById(R.id.et_clave);
        tilExtra=findViewById(R.id.til_extra);
        tilEmail=findViewById(R.id.til_correo);
        tilPassword=findViewById(R.id.til_clave);


        btnLogin= findViewById(R.id.btn_iniciar);
        btnLogin.setOnClickListener(evt -> presenter.loginWEmail());

        btnFacebook=findViewById(R.id.btn_facebook);
        btnFacebook.setOnClickListener((evt)->{
            presenter.loginWFacebook();
        });

        btnGoogle=findViewById(R.id.btn_google);
        btnGoogle.setOnClickListener(evt-> onGoogleClick());

    }

    private void onGoogleClick(){
        etCorreo.setText("");
        etClave.setText("");
        Toast.makeText(this,"Login",Toast.LENGTH_SHORT).show();

        Intent intent= new Intent(this, PaymentsActivity.class);
        startActivity(intent);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(etCorreo.getText().toString(),etClave.getText().toString());
    }

    @Override
    public void showEmailError(String error) {
        tilEmail.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        tilPassword.setError(error);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(LoginActivity.this,error,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearData() {
        tilEmail.setError("");
        etCorreo.setText("");
        tilPassword.setError("");
        etClave.setText("");
    }

    @Override
    public void openPaymentsActivity() {
        Intent intent = new Intent(this, PaymentsActivity.class);
        startActivity(intent);
    }

    @Override
    public void startWaiting() {
        piWaiting.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(false);
        btnFacebook.setEnabled(false);
        btnGoogle.setEnabled(false);
    }

    @Override
    public void stopWaiting() {
        piWaiting.setVisibility(View.GONE);
        btnLogin.setEnabled(true);
        btnFacebook.setEnabled(true);
        btnGoogle.setEnabled(true);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(EMAIL_KEY, etCorreo.getText().toString()); //Recomendado guardar claves como variables para facilidad
        outState.putString("password",etClave.getText().toString());  //Guarda la info. Antes de llamar al super

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        etCorreo.setText(savedInstanceState.getString(EMAIL_KEY));
        etClave.setText(savedInstanceState.getString("password"));
    }
}